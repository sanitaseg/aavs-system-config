#!/bin/bash
pushd aavs-system
./deploy.sh -c -v venv
popd
source aavs-system/venv/python/bin/activate
pip install -r requirements.txt

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
#make Desktop icons
for filename in Desktop/*.desktop; do
    dst_file="$HOME/Desktop/$(basename "$filename")"
    cp -rf "$filename" "$dst_file"
    gio set "$dst_file" metadata::trusted true
    chmod u+x "$dst_file"
    ln_dst_file="$HOME/.local/share/applications/$(basename "$filename")";
    ln -fs $dst_file $ln_dst_file
    gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), '$(basename "$filename")']"
    sed -i "s#/home/user/itpm-test-layer#$SCRIPT_DIR#g" $dst_file
done
