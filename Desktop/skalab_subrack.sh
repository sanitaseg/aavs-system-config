#!/bin/bash
cd "$(dirname "$0")"
source ../aavs-system/venv/python/bin/activate
cd ../aavs-system/python/utilities/skalab
dt=$(date '+%Y%m%d_%H%M%S')
python skalab_subrack.py
