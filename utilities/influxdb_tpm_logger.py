_author__ = 'Gabriele Sorrenti'

import os
import sys
import argparse
import time
import datetime
import ast
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tabulate
import yaml
import pandas
import re 

from hardware_client import WebHardwareClient
from pyaavs.station import Station

def refactor_bios(bios):
    if "v?.?.?" not in bios:
        res=re.search(r'v\s*([\d.]+)', bios)
        if res is not None:
            if len(res.group(0)) > 0:
                bios=res.group(0)
    return bios

def flatten_extend(matrix):
    flat_list = []
    for row in matrix:
        flat_list.extend(row)
    return flat_list

def flatten_dict(d):
    flattened_list = []
    for key, value in d.items():
        if isinstance(value, dict):
            _value=flatten_dict(value)
            flattened_list.extend([f"{key}.{x}" for x in _value])
        elif isinstance(value, list):
            flattened_list.extend([f"{key}.{x}" for x in value])
        else:
            if value is None:
                flattened_list.append(f"{key}")
            else:
                flattened_list.append(f"{key}.{value}")
    return flattened_list

def csv_flatten_dict(d,root_key=None):
    # print(f"csv_flatten_dict: {root_key}")
    # print(d)
    # print("====")
    flattened_list = []
    for key, value in d.items():
        if root_key is None:
            _key=key
        else:
            _key=root_key+"."+key
        # print(f"_key: {_key}")
        # print(value)
        # print("====")
        if isinstance(value, dict):
            if all(k in value for k in ('unit','exp_value','group')):
                # print("keep")
                # print(value)
                # print(value['group'])
                if isinstance(value['group'],list):
                    _groups=','.join(value['group'])
                else:
                    _groups=value['group']
                _value={'key':_key,'min':value['exp_value']['min'],'max':value['exp_value']['max'],'unit':value['unit'],'group':_groups}
                # print(value)
                flattened_list.append(_value)
            else:
                if root_key is None:
                    flattened_list.extend(csv_flatten_dict(value,key))
                else:
                    flattened_list.extend(csv_flatten_dict(value,root_key+"."+key))
        # elif isinstance(value, list):
        #     flattened_list.extend([f"{key}.{x}" for x in value])
        # else:
        #     if value is None:
        #         flattened_list.append(f"{key}")
        #     else:
        #         flattened_list.append(f"{key}.{value}")
        # print(flattened_list)
    return flattened_list

def exctract_value_from_keys(_dict,_keys):
    keys = _keys.split(".")
    _actual_dict=_dict
    for key in keys[:-1]:
        _actual_dict=_actual_dict[key]
    return _actual_dict[keys[-1]]

def format_ax(ax,axs_idx = None, subplot_row = 1):
    res={'type' : 'plot'}
    if axs_idx is None:
        _title = ax.get_title()
    else:
        _title = plot_titles[axs_idx]
    for tick in ax.get_xticklabels():
        tick.set_rotation(45)
    # print(axs_idx)
    # print(f"format_ax axs_idx: {axs_idx}")
    # print(f"format_ax _title: {_title}")
    ax.tick_params(axis='both', which='major', labelsize=6)
    ylim=None
    ytick=None
    if "temperatures" in _title:
        ylim=[20,60]
        ytick=10
        if "slot" in _title:
            ylim=[40,80]
            ytick=10
    if "fans.speed" in _title:
        ylim=[0,7000]
        ytick=2000
    # if "pps_count" in _title:
    #     ylim=[200000000-10,200000000+10]
    #     ytick=5
    if "get_adc_rms" in _title:
        ylim=[1.6,2.4]
        # res['type'] = "waterfall"
        # res['clim'] = [0,2]
    if "get_pps_delay" in _title:
        ylim=[10,24]
        ytick=2
    if ylim is not None:
        ax.set_ylim(ylim)
    if ytick is not None:
        ax.set_yticks(range(ylim[0],ylim[1]+ytick,ytick))
    if axs_idx is not None:
        if axs_idx % subplot_row == 0:
            if "slot" in _title:
                _text = f'SLOT-{int(_title.split(".")[0].split("-")[1])}'
                # print(_title)
                # print(_title.split(".")[0])
                # print(_title.split(".")[0].split("-"))
                # print(_title.split(".")[0].split("-")[1])
                _text += "\n"+tile_sn[int(_title.split(".")[0].split("-")[1])-1]
            else:
                _text = f"SUBRACK - {subrack_sn}"
                _text += "\n"+f"SMM - {smm_sn}"
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            # print(f"_text {_text}")
            ax.text(0.5, 1, _text, horizontalalignment='center',verticalalignment='bottom', bbox=props,transform = ax.transAxes, fontsize=6)
    text = ax.yaxis.get_offset_text()
    text.set_size(6)
    return res

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--subrack-ip",  default="10.0.10.32", help="provide subrack ip/hostname.")
    parser.add_argument("--subrack-port", type=int, default=8081, help="provide subrack port.")
    parser.add_argument("-c","--config", default="station_logger_config.py", help="Python file to define dictionaries for station config and monitoring points.")
    parser.add_argument("-t","--tpm-ips", nargs='+',default=[],help="provide DUT ip list instead of subrack ip.")
    parser.add_argument("-P","--program", action='store_true', default=False, help="Request program for all TPMs (implicit initialise).")
    parser.add_argument("-I","--initialise", action='store_true', default=False, help="Request initialise for all TPMs.")
    parser.add_argument("--plot-time", type=float, default=1, help="plot time in minutes.")
    parser.add_argument("--csv-output",  default=None, help="Provide path to csv log file to be created, if folder provided a file station_[subrack_ip]_[YYYY-mm-dd_HH:MM:SS].csv is created. If not provided 'log' folder is created")
    parser.add_argument("--station-output",  default=None, help="Provide path to station.yml file to be created, if folder provided a file station_[subrack_ip].yml is created. If not provided 'log' folder is created")
    parser.add_argument("-g","--get-board-info", action='store_true', default=False, help="Collect board info.")
    
    options = parser.parse_args()

    cfg = __import__(os.path.splitext(options.config)[0])

    flatten_subrack_values = flatten_dict(cfg.subrack_values)
    
    if options.program:
        options.initialise = True
    
    import influxdb_client, os, time
    from influxdb_client import InfluxDBClient, Point, WritePrecision
    from influxdb_client.client.write_api import SYNCHRONOUS

    token = os.environ.get("INFLUXDB_TOKEN")
    org = "sanitaseg"
    url = "http://localhost:8086"

    write_client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)

    bucket="server_room"

    write_api = write_client.write_api(write_options=SYNCHRONOUS)


    logtime=datetime.datetime.now()
    plt_save_time=logtime
    if options.csv_output is None:
        if not os.path.exists("log"):
            os.makedirs("log")
        options.csv_output = os.path.join("log",f"station_{options.subrack_ip}_{logtime.strftime('%Y-%m-%d_%H:%M:%S')}.csv")
    elif os.path.isdir(options.csv_output):
        options.csv_output = os.path.join(options.csv_output,f"station_{options.subrack_ip}_{logtime.strftime('%Y-%m-%d_%H:%M:%S')}.csv")

    if options.station_output is None:
        if not os.path.exists("log"):
            os.makedirs("log")
        options.station_output = os.path.join("log",f"station_{options.subrack_ip}.yml")
    elif os.path.isdir(options.station_output):
        options.station_output = os.path.join(options.station_output,f"station_{options.subrack_ip}.yml")
    
    options.plot_time = options.plot_time*60
    
    subrack = None
    tpm_slot = None

    if len(options.tpm_ips) > 0:
        tiles_ips=options.tpm_ips
        tiles_slots=list(range(1,len(options.tpm_ips)+1))
        tiles_err=[]*len(options.tpm_ips)
        options.csv_output=options.csv_output.replace(options.subrack_ip,','.join(map(str, tiles_ips)))
        options.station_output=options.station_output.replace(options.subrack_ip,','.join(map(str, tiles_ips)))
    else:
    # if options.subrack_ip is not None:
        subrack = WebHardwareClient(options.subrack_ip, int(options.subrack_port))
        if subrack.connect():
            print(f"Connect to the Subrack Webserver on {options.subrack_ip}:{options.subrack_port}")
            tpm_present = subrack.get_attribute('tpm_present')
            tpm_on_off = subrack.get_attribute('tpm_on_off')
            ips = subrack.get_attribute('assigned_tpm_ip_adds')
            board_info = subrack.get_attribute('board_info')['value']
            # print(board_info)
            if board_info['SMM']['EXT_LABEL_SN'] == "":
                smm_sn=f"{board_info['SMM']['SN']} ({board_info['SMM']['HARDWARE_REV']})"
            else:
                smm_sn=f"{board_info['SMM']['EXT_LABEL_SN']} ({board_info['SMM']['HARDWARE_REV']})"
            if board_info['SUBRACK']['EXT_LABEL'] == "":
                
                subrack_sn=f"{board_info['SUBRACK']['SN']} ({board_info['SUBRACK']['HARDWARE_REV']})"
            else:
                _ext_label=board_info['SUBRACK']['EXT_LABEL'].split(";")
                if len(_ext_label)>3:
                    subrack_sn=f"{_ext_label[1]}-{_ext_label[3]}"
                    subrack_sn=f"{subrack_sn} ({board_info['SUBRACK']['HARDWARE_REV']})"
                else:
                    subrack_sn=f"{board_info['SUBRACK']['EXT_LABEL']} ({board_info['SUBRACK']['HARDWARE_REV']})"
            tiles_ips=[]
            tiles_slots=[]
            tiles_err=[]
            n_of_tpm_present = tpm_present['value'].count(True)
            headers=['name','value']
            if n_of_tpm_present == 0:
                print("Unable to any TPM in subrack")
                sys.exit(1)
            slot = 1
            for _tpm_present in tpm_present['value']:
                if _tpm_present:
                    if tpm_on_off['value'][slot-1]:
                        tiles_ips.append(ips['value'][slot-1])
                        tiles_slots.append(slot)
                        tiles_err.append(0)
                        ip=ips['value'][slot-1]
                slot += 1
        else:
            subrack = None
            print(f"Unable to connect to the Subrack Webserver on {options.subrack_ip}:{options.subrack_port}")
            sys.exit(1)
    
    headers=['SLOT','POWER','IP','TPM_SN','ADU_SN','HARDWARE_REV','bios']
    _table=[]
    if subrack is None:
        for i in range(len(tiles_ips)):
            _table.append([i+1,"ON",tiles_ips[i]])
    else:
        for i in range(len(tpm_on_off['value'])):
            status="-" if not tpm_present['value'][i] else "ON" if tpm_on_off['value'][i] else "OFF"
            _table.append([i+1,status,ips['value'][i]])
    print()
    print(tabulate.tabulate(_table, headers=headers, tablefmt="pipe"))
    print()

    cfg.configuration['tiles'] = tiles_ips
    cfg.configuration['station']['program'] = options.program
    cfg.configuration['station']['initialise'] = options.initialise


    # print(json.dumps(cfg.configuration,indent=2))
    station = Station(cfg.configuration)

    with open(options.station_output, 'w') as yaml_file:
        yaml.dump(cfg.configuration, yaml_file, default_flow_style=False)

    # Connect station (program, initialise and configure if required)
    station.connect()

    
    subrack_err=0

    if options.get_board_info:
        for tile_idx,tile in enumerate(station.tiles):
            _board_info=tile.get_board_info()
            #print(_board_info)
            _sn="-"
            if 'EXT_LABEL_PN' in _board_info:
                if _board_info['EXT_LABEL_PN'] != "":
                    _sn=f"{_board_info['EXT_LABEL_PN']}-{_board_info['EXT_LABEL_SN']}"
            _table[tiles_slots[tile_idx]-1].append(_sn)
            #print(_table)
            _sn=_board_info['SN']
            _table[tiles_slots[tile_idx]-1].append(_sn)
            #print(_table)
            _table[tiles_slots[tile_idx]-1].append(_board_info['HARDWARE_REV'])
            _bios=_board_info['bios']
            _table[tiles_slots[tile_idx]-1].append(refactor_bios(_bios))
            #print(_table)
        
        print()
        print(tabulate.tabulate(_table, headers=headers, tablefmt="pipe"))
        print()


    

    health_status = station.tiles[0].get_health_status()
    # print(health_status)
    tpm_dict=flatten_dict(health_status)
    # print(tpm_dict)
    df = pandas.DataFrame(tpm_dict) 
    df.to_csv("tpm_health_status.csv", index=False)

    # sys.exit(0)
    num_executions = 0  # Number of times to execute the function
    total_time = {}
    groups=['whole']
    try:
        while(True):
            
            t_end = time.time() + 1
            print(".",end="")
        # for i in range(10):
            start = time.time()
            
            

            for tile_idx,tile in enumerate(station.tiles):
                points=[]
                _slot=f"slot-{tiles_slots[tile_idx]}"
                try:
                    for group in groups:
                        start_time = time.time()
                        if group == 'whole':
                            health_status = tile.get_health_status()
                        else:
                            health_status = tile.get_health_status(group=group)
                        end_time = time.time()
                        if group not in total_time:
                            total_time[group] = 0
                        total_time[group] += end_time - start_time
                    num_executions += 1
                    for key, values in cfg.station_values.items():
                        if type(values) == str:
                            _field=f"{key}.{values}"
                            _value=health_status[key][values]
                            point = (
                                    Point(options.subrack_ip)
                                    .tag("SLOT", _slot)
                                    .field(_field, _value)
                                )
                            points.append(point)
                        else:
                            for value in values:
                                _field=f"{key}.{value}"
                                _value=health_status[key][value]
                                point = (
                                    Point(options.subrack_ip)
                                    .tag("SLOT", _slot)
                                    .field(_field, _value)
                                )
                                points.append(point)
                except:
                    tiles_err[tile_idx] += 1
                    print(tiles_err)
                    
                for key,value in cfg.station_method(tile).items():
                    try:
                        _values=value()
                        if type(_values) is list:
                            for i,_value in enumerate(_values):
                                point = (
                                    Point(options.subrack_ip)
                                    .tag("SLOT", _slot)
                                    .tag("idx", i)
                                    .field(key, _value)
                                )
                                points.append(point)
                        else:
                            point = (
                                Point(options.subrack_ip)
                                .tag("SLOT", _slot)
                                .field(key, _values)
                            )
                            points.append(point)
                    except:
                        tiles_err[tile_idx] += 1
                        print(tiles_err)
                for reg in cfg.station_register:
                    try:
                        point = (
                            Point(options.subrack_ip)
                            .tag("SLOT", _slot)
                            .field(reg, tile[reg])
                        )
                        points.append(point)
                    except:
                        tiles_err[tile_idx] += 1
                        print(tiles_err)
                
                write_api.write(bucket=bucket, org="sanitaseg", record=points)
            while time.time() < t_end:
                pass
    except KeyboardInterrupt:
        pass  # Capture Ctrl+C and do nothing
    if num_executions > 0:
        for group in groups:
            average_time = total_time[group] / num_executions
            average_time_ms = round(average_time * 1000, 2)
            print(f"tile get_health_status({group}): {average_time_ms} ms ({num_executions} samples)")
    else:
        print("\nNo executions to calculate average.")

            