import os
import csv
import yaml
import argparse

def search_and_aggregate(start_folder, output_csv):
    # Initialize an empty list to store the aggregated data
    aggregated_data = []

    # Walk through the directory tree starting from the given folder
    for foldername, subfolders, filenames in os.walk(start_folder):
        for filename in filenames:
            if filename == "log.yml":
                # Form the full path to the YAML file
                file_path = os.path.join(foldername, filename)

                # Read YAML file and append its content to the list
                with open(file_path, 'r') as yaml_file:
                    try:
                        data = yaml.safe_load(yaml_file)
                        aggregated_data.append(data)
                    except yaml.YAMLError as e:
                        print(f"Error reading {file_path}: {e}")

    # Write aggregated data to a CSV file
    if aggregated_data:
        aggregated_data = sorted(aggregated_data, key=lambda x: x[list(aggregated_data[0].keys())[0]], reverse=True)
        keys = aggregated_data[0].keys()  # Assuming all YAML files have the same structure
        with open(output_csv, 'w', newline='') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=keys)
            writer.writeheader()
            writer.writerows(aggregated_data)

        print(f"Aggregated data written to {output_csv}")
    else:
        print("No 'log.yml' files found.")

def main():
    # Create a command-line argument parser
    parser = argparse.ArgumentParser(description='Search for log.yml files and aggregate their contents into a CSV file.')

    # Add command-line arguments with default values
    parser.add_argument('--start_folder', default='.', help='The starting folder for the search.')
    parser.add_argument('--output_csv', default='output.csv', help='The output CSV file.')

    # Parse the command-line arguments
    args = parser.parse_args()

    # Call the search_and_aggregate function with the provided arguments
    search_and_aggregate(args.start_folder, args.output_csv)

if __name__ == "__main__":
    main()
