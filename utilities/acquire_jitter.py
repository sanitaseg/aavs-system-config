import time
import numpy as np
import logging
from astropy.time import Time, TimeDelta
import pydaq.daq_receiver as daq
from pydaq.persisters.channel import ChannelFormatFileManager
from pydaq.persisters.aavs_file import FileModes
from pydaq.persisters import *
from pyaavs.station import Station
from pyaavs.station import load_station_configuration
import pandas as pd

filenames = []
data_received = [False]
nof_tiles = 1
timestamp = 0

def data_callback(mode, filepath, tile):
    """ 
    Callback for when a data file has been received by the DAQ.

    It stores the filename in the global 
    :param mode: string with packet mode
    :param filepath: complete filename of data file
    :param tile: tile number
    """
    global data_received
    global data
    global timestamp
    global frequency
    global adc_channels
    global seconds
    global timestamps
    global rel_jitters
    global abs_jitters
    global configuration
    if mode == "burst_channel":
        if not any(data_received):  # this is the first filename for this tile
            filenames.append(['']*nof_tiles)
        filenames[-1][tile] = filepath
        data_received[tile] = True
    if all(data_received):   # Packets for all tiles received, skip to next time
        data_received = [False]*nof_tiles
        (_timestamp, corr, phas) = correl_data(
                filenames[-1], 
                frequency, 
                adc_channels)
        _rel_jitters = np.round((np.angle(corr)/np.pi/2*1e12/frequency),3)
        _abs_jitters = np.round((np.angle(phas)/np.pi/2*1e12/frequency),3)
        print(seconds,_abs_jitters,_rel_jitters)
        if write_api is not None:
            points=[]
            print(adc_channels)
            for i,c in enumerate(adc_channels):
                point = (
                    Point("correlation")
                    .tag("SLOT", configuration['tiles'][c['tile_idx']])
                    .tag("CH", c['tile_ch'])
                    .field('abs_jitters', _abs_jitters[i])
                    )
                points.append(point)
                point = (
                    Point("correlation")
                    .tag("SLOT", configuration['tiles'][c['tile_idx']])
                    .tag("CH", c['tile_ch'])
                    .field('rel_jitters', _rel_jitters[i])
                    )
                points.append(point)
            write_api.write(bucket=bucket, org="sanitaseg", record=points)
            for filename in filenames[-1]:
                os.remove(filename)
        else:
            timestamps.append(_timestamp)
            rel_jitters.append(_rel_jitters)
            abs_jitters.append(_abs_jitters)

         

def send_channel_periodic(station, channel, duration):
    """
    Generates a channel burst packet for a specific channel every 2 seconds.

    It is used to acquire periodic samples of the channelized data stream,
    for all antennas/polarizations and for a specific frequency channel. 
    The packet is generated every 2 seconds, just after a second tick, 
    for the time indicated by the duration parameter. 

    :param station: Initialised Station object which controls the TPMs
    :param channel: the frequency channel containing the tone to acquire
    :param duration: The scan time, in seconds
    """
    global seconds
    sync_time = station.tiles[0].tpm['fpga1.pps_manager.sync_time_val']
    frame_time = 1.08e-6*256
    frame = station.tiles[0].get_fpga_timestamp()
    next_time = int(frame*frame_time) + 2
    seconds = duration
    while(seconds >= 0):
        next_frame = int(np.round(next_time/frame_time))+2
        for tile in station.tiles:
            tile.send_channelised_data(
                    1024,
                    channel, channel,
                    timestamp=next_frame, seconds=0)
        send_time = Time(sync_time, format='unix', scale= 'utc') + TimeDelta(
                next_frame*frame_time,
                format='sec',
                scale = 'tai')
        logging.info(f'Packet capture at {send_time.isot}')
        next_time = next_time + 2

        while(station.tiles[0].get_fpga_timestamp() < next_frame):
            time.sleep(0.1)
        time.sleep(0.6)
        seconds = seconds - 2

def correl_data(filenames, frequency, adc_channels):
    """
    Computes absolute and relative correlation. 

    Absolute correlation is with respect to a sinusoid at exactly 
    the specified frequency.
    Relative correlation is with respect to the reference channel,
    which is the first one in the list of ADC channels to be used.
    Correlation is normalized to 1 for perfect correlation.

    :parameter filenames: list of filenames with the hdf5 files 
        for each tile. Array of str, one per tile
    :parameter frequency: Frequency of the tone, in Hz. Absolute
        correlation works if the frequecy is an exact multiple of 1Hz.
    :param adc_channels: List of ADC input channels. In the order
        assumed in the TPMs, antenna 0 = channels (0,1).
        Channels are numbered contiguously for all tiles, with tile N
        corresponding to channels (32*N : 32*(N+1)).
    :return: tuple with 3 values: 
        timestamp in seconds, for begin of scan
        relative correlation coefficient. Complex[ADC channel]
        absolute correlation ocefficient. Complex[ADC channel]
    """
    n_tiles = len(filenames)
    freq_channel = int(round(frequency/800e6*1024))
    f0 = freq_channel * 800e6 /1024
    data = np.zeros([1024,n_tiles,32],complex)
    for t in range(n_tiles):
        #chan_file = h5py.File(f'channel_burst_{t}_{filebase}_0.hdf5','r')
        chan_file = h5py.File(filenames[t],'r')
        d = np.array(chan_file['chan_']['data'])
        d2 = d.reshape([1024,512,32])
        data[:,t,:] = d2[:,freq_channel,:]['real'] \
                 + 1j*d2[:,freq_channel,:]['imag']
        timestamp = chan_file['sample_timestamps']['data'][0,0]
        chan_file.close()
    sample_period = 1.08e-6
    (frames,last_align) = timestamp_to_frame(timestamp)
    t = (np.array(range(1024)) + frames*256) * sample_period 
    phasor = np.exp(2.*np.pi*1j * t * (f0 - frequency))
    data = data.reshape([1024,n_tiles*32])
    rel_corr = np.zeros(len(adc_channels), complex)
    norm = np.zeros(len(adc_channels), complex)
    abs_corr = np.zeros(len(adc_channels), complex)
    ref = data[:,adc_channels[0]['station_idx']]
    # for (i, c) in enumerate(adc_channels):
    for (i, c) in enumerate(adc_channels):
        norm[i] = np.sqrt(np.sum(data[:,c['station_idx']] * data[:,c['station_idx']].conj()))
        rel_corr[i] = np.sum(data[:,c['station_idx']] * ref.conj())
        abs_corr[i] = np.sum(data[:,c['station_idx']] * phasor)
    return timestamp, (rel_corr/norm/norm[0]), (abs_corr/norm)

def execute_scan(
        station, 
        frequency, 
        duration,
        adc_channels):
    """
    Executa a scan and compute the absoute and relative jitter of a tone.

    Execute a scan recording channel data every 2 seconds, for the 
    frequency channel containing te tone at the specified frequency. 
    When all data have been received, read back the hdf5 data files 
    and compute the absolute and relative jitter in the specified ADC 
    channels. Absolute jitter is computed assuming the frequency is 
    an exact (phase locked) multiple of 1 Hz. 
    Returned arrays have first index corresponding to the scans, and
    second index corresponding to the ADC channel. 

    :param station: the Station object to use to control the hardware
    :param frequency: the tone frequency, in Hz (e.g. 50e6) 
    :param duration: duration of the scan, in seconds
    :param adc_channels: list of ADC channels where the tone is expected.
            Channels are numbered contiguously for all tiles, with tile N
            corresponding to channels (32*N : 32*(N+1)).
            First element is used as a reference channel for relative jitter
    :return:    timestamps in seconds, array(float), 
                relative jitter for specified ADC channels, array
                absolute jitter for specified ADC channels, array
    """
    global data_received
    global nof_tiles
    global filenames
    global timestamps
    global rel_jitters
    global abs_jitters
    nof_tiles = len(station.tiles)
    data_received = [False] * nof_tiles
    filenames = []
    print(f"frequency {frequency}")
    freq_channel = int(round(frequency/800e6*1024))
    print(f"freq_channel {freq_channel}")
    nof_channels = len(adc_channels)
    print(f"nof_channels {nof_channels}")
    n_scans = duration//2
    timestamps = []#np.zeros(n_scans)
    rel_jitters = []#np.zeros([n_scans,nof_channels])
    abs_jitters = []#np.zeros([n_scans,nof_channels])

    print("## Initialize the DAQ receiver and perform acquisition")
    daq.initialise_daq()
    daq.start_channel_data_consumer(callback=data_callback)
    send_channel_periodic(station, freq_channel, duration)
    daq.stop_daq()

    # print("## Compute the jitter")
    # n_scans = duration//2
    # logging.info(f'Computing jitter for {n_scans} scans')
    # timestamps = np.zeros(n_scans)
    # rel_jitters = np.zeros([n_scans,nof_channels])
    # abs_jitters = np.zeros([n_scans,nof_channels])
    # for i in range(n_scans):
    #     (timestamps[i], corr, phas) = correl_data(
    #             filenames[i], 
    #             frequency, 
    #             adc_channels)
    #     rel_jitters[i] = np.angle(corr)/np.pi/2*1e12/frequency
    #     abs_jitters[i] = np.angle(phas)/np.pi/2*1e12/frequency
    # return (timestamps, rel_jitters, abs_jitters)

def timestamp_to_frame(ts):
    """ Convert a timestamp absolute time to a integer timestamp.

    The timestamps from a HDF5 file are expressed as float64, which has 15 digit
    precision. For a Unix timestamp this corresponds to us precision.
    LFAA needs ps time accuracy, so this is largely inadequate.
    As the first timestamp always occurs on a multiple of 256*1.08 us,
    and as this occurs on a second boundary every 108 seconds (TAI time), the
    absolute time can be retrieved.
    This function returns the integer number of frames and an integer timestamp
    The exact time can be derived by (last_align + frame_time * frames)
    For a tone with a frequency exactly dividing 1s only the time from the number of
    frames is required to derive its absolute phase.

    :param ts: Timestamp as a Unix time (UT time since 1970)
    :return: int. number of frames since last time frame occurred on a second boundary
             timestamp of last time on a second boundary
    """
    frame_time = 276.48e-6  # 1.08e-6*256
    tai2utc    = 37         # leap seconds
    frame_period = 390625   # frames in 108 seconds, after which frames realign with UTC
    frames = int(np.round((ts+tai2utc)/frame_time))%frame_period
    last_align = int(np.round(ts-frame_time*frames))
    return (frames, last_align)

if __name__ == "__main__":
    """
    Standalone program. 
    Connect and optionally initialises the station
    Executes the scan
    Saves results in a data file. 
    """
    from optparse import OptionParser
    from sys import argv, stdout

    global timestamps
    global rel_jitters
    global abs_jitters
    global write_api
    global configuration

    #
    # Parser accepts all parameters for station
    #
    parser = OptionParser(usage="usage: %station [options]")
    parser.add_option("--config", action="store", dest="config",
        type="str", default=None, help="Configuration file [default: None]")
    parser.add_option("--port", action="store", dest="port",
        type="int", default=None, help="Port [default: None]")
    parser.add_option("--lmc_ip", action="store", dest="lmc_ip",
        default=None, help="IP [default: None]")
    parser.add_option("--lmc_port", action="store", dest="lmc_port",
        type="int", default=None, help="Port [default: None]")
    parser.add_option("--lmc-mac", action="store", dest="lmc_mac",
        type="int", default=None, 
        help="LMC MAC address [default: None]")
    parser.add_option("-f", "--bitfile", action="store", dest="bitfile",
        default=None, 
        help="Bitfile to use (-P still required) [default: None]")
    parser.add_option("-t", "--tiles", action="store", dest="tiles",
        default=None, 
        help="Tiles to add to station [default: None]")
    parser.add_option("-P", "--program", action="store_true", dest="program",
        default=False, 
        help="Program FPGAs [default: False]")
    parser.add_option("-I", "--initialise", action="store_true", dest="initialise",
        default=False, 
        help="Initialise TPM [default: False]")
    parser.add_option("-C", "--program_cpld", action="store_true", 
        dest="program_cpld", default=False, 
        help="Update CPLD firmware (requires -f option) [default: False]")
    parser.add_option("--single_tile_mode", action="store_true", 
        dest="single_tile_mode",
        default=False, 
        help="Program all tiles as a single tile station, for testing")
    parser.add_option("-T", "--enable-test", action="store_true", dest="enable_test",
        default=False, 
        help="Enable test pattern [default: False]")
    parser.add_option("--qsfp_detection", action="store", dest="qsfp_detection",
        default=None, 
        help="Force QSFP cable detection: auto, qsfp1, qsfp2, all, none [default: auto]")
    parser.add_option("--use_teng", action="store_true", dest="use_teng",
        default=None, help="Use 10G for LMC [default: None]")
    parser.add_option("--chan-trunc", action="store", dest="chan_trunc",
        default=None, type="int", 
        help="Channeliser truncation [default: None]")
    parser.add_option("-B", "--beamf_start", action="store_true", dest="beamf_start",
        default=False, help="Start network beamformer [default: False]")
    parser.add_option("--beamformer-scaling", action="store", dest="beam_scaling",
        type="int", default=None, 
        help="Beamformer scaling [default: None]")
    parser.add_option("--beam-bandwidth", action="store", dest="beam_bandwidth",
        type="float", default=None, help="Beamformer bandwidth [default: None]")
    parser.add_option("--channel-integration-time", action="store", 
        dest="channel_integ", type="float", 
        default=None, help="Integrated channel integration time [default: None]")
    parser.add_option("--beam-integration-time", action="store", dest="beam_integ",
        type="float", default=None, 
        help="Integrated beam integration time [default: None]")

    # Specific options
    parser.add_option("--tone_frequency", action="store", 
        dest="start_frequency_channel",
        type="float", default=None, help="Tone frequency")
    parser.add_option("-i", "--daq_receiver_interface", 
        action="store", dest="daq_receiver_interface",
        default="eth0", help="Receiver interface [default: eth0]")
    parser.add_option("-d", "--daq_directory", action="store", dest="daq_directory",
        type="str", default="data",
        help="Directory for DAQ files")
    parser.add_option("--duration", action="store", dest="scan_duration",
        type=int, default=60,
        help=" Scan duration, in seconds")
    parser.add_option("--channels", action="store", dest="adc_channels",
        type="str", default="0,1,2,3",
        help="List of input ADC channels (comma separated)")
    parser.add_option("--each_tpm_channels", action="store", dest="each_tpm_channels",
        type="str", default=None,
        help="List of input ADC channels (comma separated)")
    parser.add_option("--influxdb", action="store_true", 
        default=False, 
        help="Enable influxDB log")
    parser.add_option("--ethernet_pause", 
        default=None, 
        help="Enable influxDB log")
    
    (conf, args) = parser.parse_args(argv[1:])

    write_api = None
    if conf.influxdb:
        import influxdb_client, os, time
        from influxdb_client import InfluxDBClient, Point, WritePrecision
        from influxdb_client.client.write_api import SYNCHRONOUS
        token = os.environ.get("INFLUXDB_TOKEN")
        org = "sanitaseg"
        url = "http://localhost:8086"
        write_client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)
        bucket="server_room"
        write_api = write_client.write_api(write_options=SYNCHRONOUS)
        
    # Load station configuration
    configuration = load_station_configuration(conf)
    # configuration = add_test_configuration(configuration,conf)

    frequency = configuration['observation']['start_frequency_channel']

    adc_channels = []
    for c in str.split(conf.adc_channels,','):  # configuration['adc_channels'],','):
        adc_channels.append(int(c))
    


    daq_directory = conf.daq_directory  # configuration['daq_directory']
    daq_config = {
        'receiver_interface': conf.daq_receiver_interface,  # configuration['daq_receiver_interface'],
        'directory': daq_directory,
        'nof_tiles': len(configuration['tiles']),
    }
    daq.populate_configuration(daq_config)
    station = Station(configuration)
    station.connect()

    if conf.ethernet_pause is not None:
        for tile in station.tiles:
            tile['board.regfile.ethernet_pause']=int(conf.ethernet_pause)

    if conf.each_tpm_channels is not None:
        adc_channels = []
        for idx,tile in enumerate(station.tiles):
            for c in str.split(conf.each_tpm_channels,','):
                adc_channels.append({'tile_idx':idx,'tile_ch':c,'station_idx':idx*32+int(c)})
    
    print(adc_channels)

    print("# Execute scan")
    execute_scan(
        station,
        frequency,
        conf.scan_duration,
        #  configuration['scan_duration'],
        adc_channels)

    print("# Write output files")
    outfile = "jitter_"+filenames[0][0][-21:-7]+".txt"
    data={}
    data['timestamp']=list(timestamps)
    data['abs_jitter']=list(abs_jitters)
    data['rel_jitter']=list(rel_jitters)
    df = pd.DataFrame(data)
    print(df)
    print(daq_directory+"/timestamp_"+outfile)
    print(daq_directory+"/absolute_"+outfile)
    print(daq_directory+"/relative_"+outfile)
    np.savetxt(daq_directory+"/timestamp_"+outfile, timestamps)
    np.savetxt(daq_directory+"/absolute_"+outfile, abs_jitters)
    np.savetxt(daq_directory+"/relative_"+outfile, rel_jitters)
