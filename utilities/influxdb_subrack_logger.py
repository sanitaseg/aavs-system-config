_author__ = 'Gabriele Sorrenti'

import os
import sys
import argparse
import time
import datetime
import ast
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tabulate
import yaml
import pandas

from hardware_client import WebHardwareClient
from pyaavs.station import Station

def flatten_extend(matrix):
    flat_list = []
    for row in matrix:
        flat_list.extend(row)
    return flat_list

def flatten_dict(d):
    flattened_list = []
    for key, value in d.items():
        if isinstance(value, dict):
            _value=flatten_dict(value)
            flattened_list.extend([f"{key}.{x}" for x in _value])
        elif isinstance(value, list):
            flattened_list.extend([f"{key}.{x}" for x in value])
        else:
            if value is None:
                flattened_list.append(f"{key}")
            else:
                flattened_list.append(f"{key}.{value}")
    return flattened_list

def csv_flatten_dict(d,root_key=None):
    # print(f"csv_flatten_dict: {root_key}")
    # print(d)
    # print("====")
    flattened_list = []
    for key, value in d.items():
        if root_key is None:
            _key=key
        else:
            _key=root_key+"."+key
        # print(f"_key: {_key}")
        # print(value)
        # print("====")
        if isinstance(value, dict):
            if all(k in value for k in ('unit','exp_value','group')):
                # print("keep")
                # print(value)
                # print(value['group'])
                if isinstance(value['group'],list):
                    _groups=','.join(value['group'])
                else:
                    _groups=value['group']
                _value={'key':_key,'min':value['exp_value']['min'],'max':value['exp_value']['max'],'unit':value['unit'],'group':_groups}
                # print(value)
                flattened_list.append(_value)
            else:
                if root_key is None:
                    flattened_list.extend(csv_flatten_dict(value,key))
                else:
                    flattened_list.extend(csv_flatten_dict(value,root_key+"."+key))
        # elif isinstance(value, list):
        #     flattened_list.extend([f"{key}.{x}" for x in value])
        # else:
        #     if value is None:
        #         flattened_list.append(f"{key}")
        #     else:
        #         flattened_list.append(f"{key}.{value}")
        # print(flattened_list)
    return flattened_list

def exctract_value_from_keys(_dict,_keys):
    keys = _keys.split(".")
    _actual_dict=_dict
    for key in keys[:-1]:
        _actual_dict=_actual_dict[key]
    return _actual_dict[keys[-1]]

def format_ax(ax,axs_idx = None, subplot_row = 1):
    res={'type' : 'plot'}
    if axs_idx is None:
        _title = ax.get_title()
    else:
        _title = plot_titles[axs_idx]
    for tick in ax.get_xticklabels():
        tick.set_rotation(45)
    # print(axs_idx)
    # print(f"format_ax axs_idx: {axs_idx}")
    # print(f"format_ax _title: {_title}")
    ax.tick_params(axis='both', which='major', labelsize=6)
    ylim=None
    ytick=None
    if "temperatures" in _title:
        ylim=[20,60]
        ytick=10
        if "slot" in _title:
            ylim=[40,80]
            ytick=10
    if "fans.speed" in _title:
        ylim=[0,7000]
        ytick=2000
    # if "pps_count" in _title:
    #     ylim=[200000000-10,200000000+10]
    #     ytick=5
    if "get_adc_rms" in _title:
        ylim=[1.6,2.4]
        # res['type'] = "waterfall"
        # res['clim'] = [0,2]
    if "get_pps_delay" in _title:
        ylim=[10,24]
        ytick=2
    if ylim is not None:
        ax.set_ylim(ylim)
    if ytick is not None:
        ax.set_yticks(range(ylim[0],ylim[1]+ytick,ytick))
    if axs_idx is not None:
        if axs_idx % subplot_row == 0:
            if "slot" in _title:
                _text = f'SLOT-{int(_title.split(".")[0].split("-")[1])}'
                # print(_title)
                # print(_title.split(".")[0])
                # print(_title.split(".")[0].split("-"))
                # print(_title.split(".")[0].split("-")[1])
                _text += "\n"+tile_sn[int(_title.split(".")[0].split("-")[1])-1]
            else:
                _text = f"SUBRACK - {subrack_sn}"
                _text += "\n"+f"SMM - {smm_sn}"
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            # print(f"_text {_text}")
            ax.text(0.5, 1, _text, horizontalalignment='center',verticalalignment='bottom', bbox=props,transform = ax.transAxes, fontsize=6)
    text = ax.yaxis.get_offset_text()
    text.set_size(6)
    return res

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--subrack-ip",  default="10.0.10.32", help="provide subrack ip/hostname.")
    parser.add_argument("--subrack-port", type=int, default=8081, help="provide subrack port.")
    parser.add_argument("-c","--config", default="station_logger_config.py", help="Python file to define dictionaries for station config and monitoring points.")
    parser.add_argument("-t","--tpm-ips", nargs='+',default=[],help="provide DUT ip list instead of subrack ip.")
    parser.add_argument("-P","--program", action='store_true', default=False, help="Request program for all TPMs (implicit initialise).")
    parser.add_argument("-I","--initialise", action='store_true', default=False, help="Request initialise for all TPMs.")
    parser.add_argument("--plot-time", type=float, default=1, help="plot time in minutes.")
    parser.add_argument("--csv-output",  default=None, help="Provide path to csv log file to be created, if folder provided a file station_[subrack_ip]_[YYYY-mm-dd_HH:MM:SS].csv is created. If not provided 'log' folder is created")
    parser.add_argument("--station-output",  default=None, help="Provide path to station.yml file to be created, if folder provided a file station_[subrack_ip].yml is created. If not provided 'log' folder is created")
    parser.add_argument("-G","--plot-graph", action='store_true', default=False, help="Enable influxDB use.")

    options = parser.parse_args()

    cfg = __import__(os.path.splitext(options.config)[0])

    flatten_subrack_values = flatten_dict(cfg.subrack_values)
    
    if options.program:
        options.initialise = True
    
    import influxdb_client, os, time
    from influxdb_client import InfluxDBClient, Point, WritePrecision
    from influxdb_client.client.write_api import SYNCHRONOUS

    token = os.environ.get("INFLUXDB_TOKEN")
    org = "sanitaseg"
    url = "http://localhost:8086"

    write_client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)

    bucket="server_room"

    write_api = write_client.write_api(write_options=SYNCHRONOUS)


    logtime=datetime.datetime.now()
    plt_save_time=logtime
    if options.csv_output is None:
        if not os.path.exists("log"):
            os.makedirs("log")
        options.csv_output = os.path.join("log",f"station_{options.subrack_ip}_{logtime.strftime('%Y-%m-%d_%H:%M:%S')}.csv")
    elif os.path.isdir(options.csv_output):
        options.csv_output = os.path.join(options.csv_output,f"station_{options.subrack_ip}_{logtime.strftime('%Y-%m-%d_%H:%M:%S')}.csv")

    if options.station_output is None:
        if not os.path.exists("log"):
            os.makedirs("log")
        options.station_output = os.path.join("log",f"station_{options.subrack_ip}.yml")
    elif os.path.isdir(options.station_output):
        options.station_output = os.path.join(options.station_output,f"station_{options.subrack_ip}.yml")
    
    options.plot_time = options.plot_time*60
    
    subrack = None
    tpm_slot = None

    if len(options.tpm_ips) > 0:
        tiles_ips=options.tpm_ips
        tiles_slots=list(range(1,len(options.tpm_ips)+1))
        tiles_err=[]*len(options.tpm_ips)
        options.csv_output=options.csv_output.replace(options.subrack_ip,','.join(map(str, tiles_ips)))
        options.station_output=options.station_output.replace(options.subrack_ip,','.join(map(str, tiles_ips)))
    else:
    # if options.subrack_ip is not None:
        subrack = WebHardwareClient(options.subrack_ip, int(options.subrack_port))
        if subrack.connect():
            print(f"Connect to the Subrack Webserver on {options.subrack_ip}:{options.subrack_port}")
            board_info = subrack.get_attribute('board_info')['value']
            # print(board_info)
            if board_info['SMM']['EXT_LABEL_SN'] == "":
                smm_sn=f"{board_info['SMM']['SN']} ({board_info['SMM']['HARDWARE_REV']})"
            else:
                smm_sn=f"{board_info['SMM']['EXT_LABEL_SN']} ({board_info['SMM']['HARDWARE_REV']})"
            if board_info['SUBRACK']['EXT_LABEL'] == "":
                
                subrack_sn=f"{board_info['SUBRACK']['SN']} ({board_info['SUBRACK']['HARDWARE_REV']})"
            else:
                _ext_label=board_info['SUBRACK']['EXT_LABEL'].split(";")
                if len(_ext_label)>3:
                    subrack_sn=f"{_ext_label[1]}-{_ext_label[3]}"
                    subrack_sn=f"{subrack_sn} ({board_info['SUBRACK']['HARDWARE_REV']})"
                else:
                    subrack_sn=f"{board_info['SUBRACK']['EXT_LABEL']} ({board_info['SUBRACK']['HARDWARE_REV']})"
        else:
            subrack = None
            print(f"Unable to connect to the Subrack Webserver on {options.subrack_ip}:{options.subrack_port}")
            sys.exit(1)
    
    subrack_err=0

    res = subrack.execute_command(command="get_health_dictionary")
    subrack_dict=csv_flatten_dict(res['retvalue'])
    for _dict in subrack_dict:
        for key in ['min','max']:
            if _dict[key] is None:
                _dict.update({key: None, key: '-'})
    df = pandas.DataFrame(subrack_dict) 
    df.to_csv("subrack_health_status.csv", index=False)

    num_executions = 0  # Number of times to execute the function
    total_time = 0
    try:
        while(True):
            points=[]
            t_end = time.time() + 1
            print(".",end="")
        # for i in range(10):
            start = time.time()
            
            if subrack is not None:
                start_time = time.time()
                status = "BUSY"
                while True:
                    res = subrack.execute_command(command="get_health_status")
                    if res['status'] != "BUSY":
                        break
                    time.sleep(0.01)
                    print("*",end="")
                end_time = time.time()
                total_time += end_time - start_time
                num_executions += 1
                total_power = 0
                for keys in flatten_subrack_values:
                    try:
                        value = exctract_value_from_keys(res['retvalue'],keys)
                    except Exception as e: 
                        print(e)
                        print(res)
                        raise("Exception")
                    if 'psus.power_in' in keys:
                        total_power += value
                    point = (
                        Point(options.subrack_ip)
                        .tag("SLOT", "subrack")
                        .field(keys, value)
                    )
                    points.append(point)
                point = (
                            Point(options.subrack_ip)
                            .tag("SLOT", "subrack")
                            .field('psus.power_in.total', total_power)
                        )
                points.append(point)
            write_api.write(bucket=bucket, org="sanitaseg", record=points)
            while time.time() < t_end:
                pass
    except KeyboardInterrupt:
        pass  # Capture Ctrl+C and do nothing
    if num_executions > 0:
        average_time = total_time / num_executions
        average_time_ms = round(average_time * 1000, 2)
        print(f"\nAverage execution time of subrack get_health_status: {average_time_ms} ms ({num_executions} samples)")
    else:
        print("\nNo executions to calculate average.")