_author__ = 'Gabriele Sorrenti'

import os
import sys
import argparse
import time
import datetime
import ast
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tabulate
import yaml
import pandas

from hardware_client import WebHardwareClient
from pyaavs.station import Station

def flatten_extend(matrix):
    flat_list = []
    for row in matrix:
        flat_list.extend(row)
    return flat_list

def flatten_dict(d):
    flattened_list = []
    for key, value in d.items():
        if isinstance(value, dict):
            _value=flatten_dict(value)
            flattened_list.extend([f"{key}.{x}" for x in _value])
        elif isinstance(value, list):
            flattened_list.extend([f"{key}.{x}" for x in value])
        else:
            if value is None:
                flattened_list.append(f"{key}")
            else:
                flattened_list.append(f"{key}.{value}")
    return flattened_list

def csv_flatten_dict(d,root_key=None):
    # print(f"csv_flatten_dict: {root_key}")
    # print(d)
    # print("====")
    flattened_list = []
    for key, value in d.items():
        if root_key is None:
            _key=key
        else:
            _key=root_key+"."+key
        # print(f"_key: {_key}")
        # print(value)
        # print("====")
        if isinstance(value, dict):
            if all(k in value for k in ('unit','exp_value','group')):
                # print("keep")
                # print(value)
                # print(value['group'])
                if isinstance(value['group'],list):
                    _groups=','.join(value['group'])
                else:
                    _groups=value['group']
                _value={'key':_key,'min':value['exp_value']['min'],'max':value['exp_value']['max'],'unit':value['unit'],'group':_groups}
                # print(value)
                flattened_list.append(_value)
            else:
                if root_key is None:
                    flattened_list.extend(csv_flatten_dict(value,key))
                else:
                    flattened_list.extend(csv_flatten_dict(value,root_key+"."+key))
        # elif isinstance(value, list):
        #     flattened_list.extend([f"{key}.{x}" for x in value])
        # else:
        #     if value is None:
        #         flattened_list.append(f"{key}")
        #     else:
        #         flattened_list.append(f"{key}.{value}")
        # print(flattened_list)
    return flattened_list

def exctract_value_from_keys(_dict,_keys):
    keys = _keys.split(".")
    _actual_dict=_dict
    for key in keys[:-1]:
        _actual_dict=_actual_dict[key]
    return _actual_dict[keys[-1]]

def format_ax(ax,axs_idx = None, subplot_row = 1):
    res={'type' : 'plot'}
    if axs_idx is None:
        _title = ax.get_title()
    else:
        _title = plot_titles[axs_idx]
    for tick in ax.get_xticklabels():
        tick.set_rotation(45)
    # print(axs_idx)
    # print(f"format_ax axs_idx: {axs_idx}")
    # print(f"format_ax _title: {_title}")
    ax.tick_params(axis='both', which='major', labelsize=6)
    ylim=None
    ytick=None
    if "temperatures" in _title:
        ylim=[20,60]
        ytick=10
        if "slot" in _title:
            ylim=[40,80]
            ytick=10
    if "fans.speed" in _title:
        ylim=[0,7000]
        ytick=2000
    # if "pps_count" in _title:
    #     ylim=[200000000-10,200000000+10]
    #     ytick=5
    if "get_adc_rms" in _title:
        ylim=[1.6,2.4]
        # res['type'] = "waterfall"
        # res['clim'] = [0,2]
    if "get_pps_delay" in _title:
        ylim=[10,24]
        ytick=2
    if ylim is not None:
        ax.set_ylim(ylim)
    if ytick is not None:
        ax.set_yticks(range(ylim[0],ylim[1]+ytick,ytick))
    if axs_idx is not None:
        if axs_idx % subplot_row == 0:
            if "slot" in _title:
                _text = f'SLOT-{int(_title.split(".")[0].split("-")[1])}'
                # print(_title)
                # print(_title.split(".")[0])
                # print(_title.split(".")[0].split("-"))
                # print(_title.split(".")[0].split("-")[1])
                _text += "\n"+tile_sn[int(_title.split(".")[0].split("-")[1])-1]
            else:
                _text = f"SUBRACK - {subrack_sn}"
                _text += "\n"+f"SMM - {smm_sn}"
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            # print(f"_text {_text}")
            ax.text(0.5, 1, _text, horizontalalignment='center',verticalalignment='bottom', bbox=props,transform = ax.transAxes, fontsize=6)
    text = ax.yaxis.get_offset_text()
    text.set_size(6)
    return res

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--subrack-ip",  default="10.0.10.32", help="provide subrack ip/hostname.")
    parser.add_argument("--subrack-port", type=int, default=8081, help="provide subrack port.")
    parser.add_argument("-c","--config", default="station_logger_config.py", help="Python file to define dictionaries for station config and monitoring points.")
    parser.add_argument("-t","--tpm-ips", nargs='+',default=[],help="provide DUT ip list instead of subrack ip.")
    parser.add_argument("-P","--program", action='store_true', default=False, help="Request program for all TPMs (implicit initialise).")
    parser.add_argument("-I","--initialise", action='store_true', default=False, help="Request initialise for all TPMs.")
    parser.add_argument("--plot-time", type=float, default=1, help="plot time in minutes.")
    parser.add_argument("--csv-output",  default=None, help="Provide path to csv log file to be created, if folder provided a file station_[subrack_ip]_[YYYY-mm-dd_HH:MM:SS].csv is created. If not provided 'log' folder is created")
    parser.add_argument("--station-output",  default=None, help="Provide path to station.yml file to be created, if folder provided a file station_[subrack_ip].yml is created. If not provided 'log' folder is created")
    options = parser.parse_args()

    cfg = __import__(os.path.splitext(options.config)[0])

    flatten_subrack_values = flatten_dict(cfg.subrack_values)
    
    if options.program:
        options.initialise = True

    logtime=datetime.datetime.now()
    plt_save_time=logtime
    if options.csv_output is None:
        if not os.path.exists("log"):
            os.makedirs("log")
        options.csv_output = os.path.join("log",f"station_{options.subrack_ip}_{logtime.strftime('%Y-%m-%d_%H:%M:%S')}.csv")
    elif os.path.isdir(options.csv_output):
        options.csv_output = os.path.join(options.csv_output,f"station_{options.subrack_ip}_{logtime.strftime('%Y-%m-%d_%H:%M:%S')}.csv")

    if options.station_output is None:
        if not os.path.exists("log"):
            os.makedirs("log")
        options.station_output = os.path.join("log",f"station_{options.subrack_ip}.yml")
    elif os.path.isdir(options.station_output):
        options.station_output = os.path.join(options.station_output,f"station_{options.subrack_ip}.yml")
    
    options.plot_time = options.plot_time*60
    
    subrack = None
    tpm_slot = None

    if len(options.tpm_ips) > 0:
        tiles_ips=options.tpm_ips
        tiles_slots=list(range(1,len(options.tpm_ips)+1))
        tiles_err=[]*len(options.tpm_ips)
        options.csv_output=options.csv_output.replace(options.subrack_ip,','.join(map(str, tiles_ips)))
        options.station_output=options.station_output.replace(options.subrack_ip,','.join(map(str, tiles_ips)))
    else:
    # if options.subrack_ip is not None:
        subrack = WebHardwareClient(options.subrack_ip, int(options.subrack_port))
        if subrack.connect():
            print(f"Connect to the Subrack Webserver on {options.subrack_ip}:{options.subrack_port}")
            tpm_present = subrack.get_attribute('tpm_present')
            tpm_on_off = subrack.get_attribute('tpm_on_off')
            ips = subrack.get_attribute('assigned_tpm_ip_adds')
            board_info = subrack.get_attribute('board_info')['value']
            # print(board_info)
            if board_info['SMM']['EXT_LABEL_SN'] == "":
                smm_sn=f"{board_info['SMM']['SN']} ({board_info['SMM']['HARDWARE_REV']})"
            else:
                smm_sn=f"{board_info['SMM']['EXT_LABEL_SN']} ({board_info['SMM']['HARDWARE_REV']})"
            if board_info['SUBRACK']['EXT_LABEL'] == "":
                
                subrack_sn=f"{board_info['SUBRACK']['SN']} ({board_info['SUBRACK']['HARDWARE_REV']})"
            else:
                _ext_label=board_info['SUBRACK']['EXT_LABEL'].split(";")
                if len(_ext_label)>3:
                    subrack_sn=f"{_ext_label[1]}-{_ext_label[3]}"
                    subrack_sn=f"{subrack_sn} ({board_info['SUBRACK']['HARDWARE_REV']})"
                else:
                    subrack_sn=f"{board_info['SUBRACK']['EXT_LABEL']} ({board_info['SUBRACK']['HARDWARE_REV']})"
            tiles_ips=[]
            tiles_slots=[]
            tiles_err=[]
            n_of_tpm_present = tpm_present['value'].count(True)
            headers=['name','value']
            if n_of_tpm_present == 0:
                print("Unable to any TPM in subrack")
                sys.exit(1)
            slot = 1
            for _tpm_present in tpm_present['value']:
                if _tpm_present:
                    if tpm_on_off['value'][slot-1]:
                        tiles_ips.append(ips['value'][slot-1])
                        tiles_slots.append(slot)
                        tiles_err.append(0)
                        ip=ips['value'][slot-1]
                slot += 1
        else:
            subrack = None
            print(f"Unable to connect to the Subrack Webserver on {options.subrack_ip}:{options.subrack_port}")
            sys.exit(1)
    
    headers=['SLOT','POWER','IP']
    _table=[]
    if subrack is None:
        for i in range(len(tiles_ips)):
            _table.append([i+1,"ON",tiles_ips[i]])
    else:
        for i in range(len(tpm_on_off['value'])):
            status="-" if not tpm_present['value'][i] else "ON" if tpm_on_off['value'][i] else "OFF"
            _table.append([i+1,status,ips['value'][i]])
    print()
    print(tabulate.tabulate(_table, headers=headers, tablefmt="pipe"))
    print()

    cfg.configuration['tiles'] = tiles_ips
    cfg.configuration['station']['program'] = options.program
    cfg.configuration['station']['initialise'] = options.initialise


    # print(json.dumps(cfg.configuration,indent=2))
    station = Station(cfg.configuration)

    with open(options.station_output, 'w') as yaml_file:
        yaml.dump(cfg.configuration, yaml_file, default_flow_style=False)

    # Connect station (program, initialise and configure if required)
    station.connect()

    header=["time"]
    if subrack is not None:
        subrack_header = flatten_subrack_values
        header.extend(subrack_header)

    tiles_header = []
    tile_sn = [""]*8
    for idx,tile in enumerate(station.tiles):
        board_info = tile.get_board_info()
        if board_info['EXT_LABEL_SN'] == "":
            tile_sn[tiles_slots[idx]-1]=f"{board_info['SN']} ({board_info['HARDWARE_REV']})"
        else:
            tile_sn[tiles_slots[idx]-1]=f"{board_info['EXT_LABEL_SN']} ({board_info['HARDWARE_REV']})"
        tile_header=[]
        for key, values in cfg.station_values.items():
            if type(values) == str:
                tile_header.append(f"slot-{tiles_slots[idx]}.{key}.{values}")
            else:
                for value in values:
                    tile_header.append(f"slot-{tiles_slots[idx]}.{key}.{value}")

        for key in cfg.station_method(station.tiles[0]):
            tile_header.append(f"slot-{tiles_slots[idx]}.{key}")

        for reg in cfg.station_register:
            tile_header.append(f"slot-{tiles_slots[idx]}.{reg}")
        tiles_header.append(tile_header)
        header.extend(tile_header)
        

    with open(options.csv_output,'w') as fd:
        fd.write(','.join(header)+"\n")
        fd.close()

        subplot_row=max((len(header)-1-len(flatten_subrack_values))//len(station.tiles), len(flatten_subrack_values))
        subplot_clm=len(station.tiles)+1
    plot_titles = [[""]*subplot_row]*subplot_clm
    fig, axs = plt.subplots(subplot_row,subplot_clm,sharex=True)#, sharey='row')
    # fig.set_figwidth(3*subplot_row)
    # fig.set_figheight(5*subplot_clm)
    
    # plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
    fig.canvas.manager.set_window_title(options.subrack_ip)
    for i_row in range(subplot_row):
        for i_clm in range(subplot_clm):
            ax=axs[i_row][i_clm]
            format_ax(ax)
            ax.grid(axis="x")
            ax.grid(axis="y")
    fig.tight_layout()
    plt.subplots_adjust(wspace=0.2, hspace=0.3)
    # plt.margins(x=5, y=5)
    axs = flatten_extend(list(map(list, zip(*axs))))
    plot_titles = flatten_extend(list(map(list, zip(*plot_titles))))
    # print(len(axs))
    if subrack is not None:
        for idx,_value in enumerate(subrack_header):
            ax=axs[idx]
            # print(f"set_title: {idx} {_value}")
            ax.set_title(_value,fontsize = 6, x=0.02, y=0.02, va="top", ha="left")
            plot_titles[idx] = _value
        # print(f"subplot_row: {subplot_row}")
    for tile_idx,tile_row in enumerate(tiles_header):
        for idx,_value in enumerate(tile_row):
            # print(f"tile_idx: {tile_idx}")
            # print(f"idx: {idx}")
            # print(f"set_title: {idx+subplot_row*(tile_idx+1)} {_value}")
            axs_idx=idx+subplot_row*(tile_idx+1)
            ax=axs[axs_idx]
            ax.set_title(_value.split('.', 1)[1],fontsize = 6, x=0.02, y=0.02, va="top", ha="left")
            plot_titles[axs_idx] = _value
            format_ax(ax,axs_idx, subplot_row)

        

    
    
    # for i in range(len(header)-1):
    #     ax=axs[i]
    #     ax.set_title(header[i+1],fontsize = 6, x=0.02, y=0.8, va="top", ha="left")
    #     ax.grid(axis="x")
    #     ax.grid(axis="y")
    fig.canvas.draw()
    plt.show(block=False)
    t=[]
    data=[[] for i in range(len(header)-1)]
    subrack_err=0

    # # GENERATE CSV with all monitoring points for documentation pourpose
    # res = subrack.execute_command(command="get_health_dictionary")
    # subrack_dict=csv_flatten_dict(res['retvalue'])
    # for _dict in subrack_dict:
    #     for key in ['min','max']:
    #         if _dict[key] is None:
    #             _dict.update({key: None, key: '-'})
    # df = pandas.DataFrame(subrack_dict) 
    # df.to_csv("subrack_health_status.csv", index=False)

    # health_status = station.tiles[0].get_health_status()
    # # print(health_status)
    # tpm_dict=flatten_dict(health_status)
    # # print(tpm_dict)
    # df = pandas.DataFrame(tpm_dict) 
    # df.to_csv("tpm_health_status.csv", index=False)

    while(True):
    # for i in range(10):
        start = time.time()
        now=datetime.datetime.now()
        row=[now.strftime("%Y-%m-%d %H:%M:%S")]
        
        subrack_row=[]
        if subrack is not None:
            try:
                res = subrack.execute_command(command="get_health_status")
                for keys in flatten_subrack_values:
                    value = exctract_value_from_keys(res['retvalue'],keys)
                    subrack_row.append(f"{value}")
            except:
                subrack_err += 1
                for keys in flatten_subrack_values:
                    subrack_row.append(f"{float('nan')}")

        row.extend(subrack_row)
        tiles_row=[]
        for tile_idx,tile in enumerate(station.tiles):
            tile_row = []
            try:
                health_status = tile.get_health_status()
                for key, values in cfg.station_values.items():
                    if type(values) == str:
                        tile_row.append(f"{health_status[key]}")
                    else:
                        for value in values:
                            tile_row.append(f"{health_status[key][value]}")
            except:
                tiles_err[tile_idx] += 1
                print("tiles_err")
                print(tiles_err)
                for key, values in cfg.station_values.items():
                    if type(values) == str:
                        tile_row.append(f"{float('nan')}")
                    else:
                        for value in values:
                            tile_row.append(f"{float('nan')}")
            end  = time.time()
            
            for key,value in cfg.station_method(tile).items():
                tile_row.append(f"{value()}")

            for reg in cfg.station_register:
                tile_row.append(f"{tile[reg]}")

            tiles_row.append(tile_row)
            row.extend(tile_row)

        with open(options.csv_output,'a') as fd:
            fd.write(','.join(row)+"\n")
            fd.close()

        t.append(now)
        if len(t) > options.plot_time:
            del t[0]
        j=0
        for axs_idx,_value in enumerate(subrack_row):
            ax=axs[axs_idx]
            if _value.lower() == "false":
                data[j].append(0)
            elif _value.lower() == "true":
                data[j].append(1)
            else:
                data[j].append(ast.literal_eval(row[j+1]))
            if len(data[j]) > options.plot_time:
                del data[j][0]
            ax.plot(t,data[j],'r-',linewidth=0.5)
            ax.set_xlim([now-datetime.timedelta(seconds=options.plot_time), now])
            format_ax(ax,axs_idx, subplot_row)
            j += 1
        # print(f"subplot_row: {subplot_row}")
        for tile_idx,tile_row in enumerate(tiles_row):
            for idx,_value in enumerate(tile_row):
                axs_idx = idx+subplot_row*(tile_idx+1)
                ax=axs[axs_idx]
                _format_ax=format_ax(ax,axs_idx, subplot_row)
                # print(f"========================================")
                # print(f"tile_idx: {tile_idx}")
                # print(f"idx: {idx}")
                # print(f"subplot: {subplot_row*(tile_idx+1)}")
                # print(f"plot_titles: {plot_titles[axs_idx]}")
                # print(f"_format_ax: {_format_ax}")
                if _value.lower() == "false":
                    data[j].append(0)
                elif _value.lower() == "true":
                    data[j].append(1)
                else:
                    try:
                        data[j].append(ast.literal_eval(row[j+1]))
                    except:
                        print(f"exception: {tile_idx}")
                        print(row[j+1])
                        data[j].append(float('nan'))
                if len(data[j]) > options.plot_time:
                    del data[j][0]
                if type(data[j][-1]) is list:
                    # print(f"{ax.get_title()} {len(data[j][-1])} {data[j][-1]}")
                    y = np.transpose(np.asarray(data[j]))
                    cmap = matplotlib.pyplot.colormaps['hsv']
                    # Take colors at regular intervals spanning the colormap.
                    colors = cmap(np.linspace(0, 1, len(data[j][-1])))
                    if _format_ax['type'] == 'waterfall':
                        ax.imshow(y,extent=[t[0], t[-1], 0, len(data[j][-1])-1],interpolation='none', aspect='auto', cmap='winter', origin ='lower', clim =_format_ax['clim'])
                        # plt.colorbar(c) 
                    else:
                        for iii,color in enumerate(colors):
                            ax.plot(t,y[iii],linewidth=0.5,color=color)
                else:
                    ax.plot(t,data[j],'r-',linewidth=0.5)
                ax.set_xlim([now-datetime.timedelta(seconds=options.plot_time), now])
                _format_ax=format_ax(ax,axs_idx, subplot_row)
                j += 1
                

        fig.canvas.draw()
        plt.pause(0.1)
        if now-plt_save_time > datetime.timedelta(seconds=options.plot_time):
            plt_save_time = now
            _file=options.csv_output.replace(".csv",f"_{now.strftime('%Y-%m-%d_%H:%M:%S')}.png")
            plt.savefig(_file,dpi=300)

        while(end-start<1):
            time.sleep(0.002)
            end  = time.time()
        start = end - 0.001
