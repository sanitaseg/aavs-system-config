import h5py
import argparse

def get_available_datasets(file_path):
    datasets = []

    def append_dataset(name, obj):
        if isinstance(obj, h5py.Dataset):
            datasets.append(name)

    with h5py.File(file_path, 'r') as file:
        file.visititems(append_dataset)

    return datasets

def print_dataset_info(file_path, dataset_name):
    with h5py.File(file_path, 'r') as file:
        # Check if the dataset exists
        if dataset_name not in file:
            print(f"Dataset '{dataset_name}' not found in the HDF5 file.")
            return

        # Get the dataset
        dataset = file[dataset_name]

        # Print dataset information
        print(f"Dataset: {dataset_name}")
        print("Shape:", dataset.shape)
        print("Datatype:", dataset.dtype)
        print("Attributes:")
        for attr_name, attr_value in dataset.attrs.items():
            print(f"- {attr_name}: {attr_value}")

        # Print dataset content
        print("Content:")
        print(dataset[()])

if __name__ == "__main__":
    # Set up command-line argument parsing
    parser = argparse.ArgumentParser(description="Print information about datasets in an HDF5 file.")
    parser.add_argument("hdf5_file", help="Path to the HDF5 file")

    # Parse command-line arguments
    args = parser.parse_args()
    hdf5_file_path = args.hdf5_file

    # Get available datasets
    available_datasets = get_available_datasets(hdf5_file_path)

    # Display available datasets
    print("Available datasets:")
    for dataset in available_datasets:
        print(f"- {dataset}")

    # Ask the user to choose a dataset
    dataset_name = input("Enter the name of the dataset you want to print information about: ")

    # Print information about and the content of the chosen dataset
    print_dataset_info(hdf5_file_path, dataset_name)
