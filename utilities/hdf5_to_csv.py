import argparse
import os
import h5py
import pandas as pd

def hdf5_to_csv(input_file, output_file):
    """
    Convert HDF5 file to CSV file.

    Parameters:
    - input_file (str): Path to the input HDF5 file.
    - output_file (str): Path to the output CSV file.
    """
    try:
        # Read HDF5 file
        with h5py.File(input_file, 'r') as hdf5_file:
            # Extract data from HDF5 file
            data = {}
            hdf5_file.visititems(lambda name, obj: data.update({name: obj.value}))

        # Convert data to a Pandas DataFrame
        df = pd.DataFrame(data)

        # If output file is not provided, create one using the input file's path
        if not output_file:
            output_file = os.path.splitext(input_file)[0] + '.csv'

        # Write DataFrame to CSV file
        df.to_csv(output_file, index=False)
        
        print(f"Conversion successful. CSV file saved at: {output_file}")

    except Exception as e:
        print(f"Error: {e}")

def main():
    parser = argparse.ArgumentParser(description='Convert HDF5 file to CSV file.')
    parser.add_argument('input_file', help='Path to the input HDF5 file.')
    parser.add_argument('--output_file', help='Path to the output CSV file. If not provided, the output file will be created in the same directory as the input file with the same name and a .csv extension.')

    args = parser.parse_args()
    
    hdf5_to_csv(args.input_file, args.output_file)

if __name__ == "__main__":
    main()
