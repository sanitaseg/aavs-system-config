_author__ = 'Bubs'

import os
import sys
from optparse import OptionParser
import time
from datetime import datetime
import subprocess
import art
import yaml
import signal

def killall_python():
    pids = subprocess.check_output(["pgrep", "python"])
    p = subprocess.run(["pgrep", "python"], capture_output=True, text=True)
    pids=p.stdout
    pids=str(pids).split("\n")
    for pid in pids:
        if pid != '':
            if int(pid) != int(os.getpid()):
                print(f"kill {pid}")
                subprocess.Popen(["kill",pid])

def signal_handler(sig, frame):
        print("CTRL+C pressed. Aborting...")
        killall_python()
        sys.exit(0)

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-n","--name", default="emi_test",help="")
    parser.add_option("--directory", default="/storage/emi_test",help="")
    parser.add_option("--link", default="/storage/skalab",help="")
    parser.add_option("-m","--mode", default=None,help="")
    parser.add_option("--aavs_location", default="../aavs-system",help="")
    parser.add_option("--venv_location", default="../aavs-system/venv/python",help="")
    parser.add_option("-x","--old_run", action = 'store_true', default=False,help="")
    (opt, args) = parser.parse_args()

    
    
    cabinet_profiles = {
        #'c1s1' : {'profile':"cabinet1_subrack1",'station':"/home/user/.skalab/cabinet1_subrack1/station.yml"},
        #'c1s2' : {'profile':"cabinet1_subrack2",'station':"/home/user/.skalab/cabinet1_subrack2/station.yml"},
        #'c1s3' : {'profile':"cabinet1_subrack3",'station':"/home/user/.skalab/cabinet1_subrack3/station.yml"},
        #'c1s4' : {'profile':"cabinet1_subrack4",'station':"/home/user/.skalab/cabinet1_subrack4/station.yml"},

        'c2s1' : {'profile':"cabinet2_subrack1",'station':"/home/user/.skalab/cabinet2_subrack1/station.yml"},
        'c2s2' : {'profile':"cabinet2_subrack2",'station':"/home/user/.skalab/cabinet2_subrack2/station.yml"},
        'c2s3' : {'profile':"cabinet2_subrack3",'station':"/home/user/.skalab/cabinet2_subrack3/station.yml"},
        'c2s4' : {'profile':"cabinet2_subrack4",'station':"/home/user/.skalab/cabinet2_subrack4/station.yml"},
    }

    

    # killall_python()
    
    print("Wait for processes to restarts (CTRL+C to abort)")
    time.sleep(3)



    #opt.link=profiles[opt.mode]['link']
 

    run=1
    ls_dir = os.listdir(opt.directory)
    for x in ls_dir:
        try:
            _run = int(x.split("_")[0])
        except:
            _run = None
        if _run is not None:
            if opt.old_run:
                if _run > run:
                    run = _run
                    run_name = x
            else:
                run = max(run,_run+1)
    if not opt.old_run:
        _r={}
        _r['run']=run
        _r['now']=datetime.utcnow()
        strftime = _r['now'].strftime("%Y-%m-%d_%H%M%S")
        _r['name']=opt.name
        _r['mode']=opt.mode
#        _r['profile']=profile['profile']
#        _r['station']=profile['station']
        _r['result']=""
        _r['note']=""
        _r['manual_action']=""
        run_name = f"{run}_{strftime}_{opt.name}_{opt.mode}" #_{profile['profile']}"
    new_dir= os.path.join(opt.directory,run_name)
    actual_dir= opt.link
    yaml_file = os.path.join(new_dir,'log.yml')
    
    if not opt.old_run:
        os.mkdir(new_dir)
        print("ACTUAL_DIR");     
        print(actual_dir)

        for subfolder in ["integrated_channels_test","log","data","data/temperatures","data/daq","data/csv","data/live","data/live/log","integrated_spectra","tpm_monitor"]:
            if not os.path.exists(os.path.join(new_dir,subfolder)):
                os.mkdir(os.path.join(new_dir,subfolder))
        #if os.path.exists(actual_dir):
        #    if os.path.islink(actual_dir):
        #        os.remove(actual_dir)
        #    else:
        #        print("ERROR - actual folder is not a symlink)")
        #        print(actual_dir)
        #        sys.exit(1)
        print("NEW DIR");
        print(new_dir)   
        print("ACTUAL_DIR");     
        print(actual_dir)
        #os.symlink(new_dir,actual_dir)
        # new_pictures_dir = os.path.join(actual_dir,"Pictures")
        # pictures_dir = os.path.expanduser("~/Pictures")
        # os.mkdir(new_pictures_dir)
        # #print(new_pictures_dir)
        # #print(pictures_dir)
        # print(os.path.expanduser("~/Pictures"))
        # if os.path.exists(pictures_dir):
        #     if os.path.islink(pictures_dir):
        #         os.remove(pictures_dir)
        #     else:
        #         print("ERROR - actual folder is not a symlink)")
        #         print(pictures_dir)
        #         sys.exit(1)
        # os.symlink(new_pictures_dir,pictures_dir)

        # new_monitor_dir = os.path.join(actual_dir,"monitor_log")
        # monitor_dir = os.path.expanduser("~/.skalab/LOG/log")
        # os.mkdir(new_monitor_dir)
        # print(pictures_dir)
        # print(os.path.expanduser("~/Pictures"))
        # if os.path.exists(monitor_dir):
        #     if os.path.islink(pictures_dir):
        #         os.remove(monitor_dir)
        #     else:
        #         print("ERROR - actual folder is not a symlink)")
        #         print(monitor_dir)
        #         sys.exit(1)
        # os.symlink(new_monitor_dir,monitor_dir)
    
    if not os.path.exists(yaml_file): 
        with open(yaml_file, 'w') as outfile:
            yaml.dump(_r, outfile, default_flow_style=False, sort_keys=False)
    
    skalab_path = os.path.join(opt.aavs_location,"python/utilities/skalab")
    emc_path = os.path.join(opt.aavs_location,"python/utilities/emc")
    venv_python=os.path.abspath(os.path.join(opt.venv_location,"bin/python"))
    for prof in cabinet_profiles:
        print(f"{prof}")
        cmds=[
        #{'arg':[venv_python,"skalab_monitor.py",'--connect','-p',profile['profile']],'cwd':skalab_path},
        {'arg':[venv_python,"skalab_subrack.py",'--connect',"--profile",cabinet_profiles[prof]['profile']],'cwd':skalab_path},
        #{'arg':[venv_python,"skalab_live.py",'--connect',"--profile",profile['profile']],'cwd':skalab_path},
        #{'arg':[venv_python,"emc_live.py","--config",profile['station'],"--directory",os.path.join(actual_dir,"integrated_channels_test")],'cwd':emc_path},
        #{'arg':["gedit",yaml_file],'cwd':skalab_path},
#        {'arg':["python","log_to_csv.py","--start_folder",opt.directory,"--output_csv",os.path.join(opt.directory,"aggregated_log.csv")],'cwd':os.path.dirname(__file__)},
        ]
        for _cmd in cmds:
            print(" ".join(_cmd['arg']))
            file_out=open(os.path.join(new_dir,_cmd['arg'][1]+".out"),"a")
            subprocess.Popen(_cmd['arg'],cwd=_cmd['cwd'],stdout=file_out)

    
    #print(art.text2art(f"{run} - {opt.name} - {opt.mode} - {cabinet_profiles['profile']}"))
    #print("Log available at:")
    #print(new_dir)

    # # Set up the signal handler for CTRL+C
    # signal.signal(signal.SIGINT, signal_handler)

    # while(True):
    #     print(art.text2art(f"{run} - {opt.name} - {opt.mode} - {profile['profile']}"))
    #     print("Log available at:")
    #     print(new_dir)
    #     print("Press CTRL+C to kill all python processes")
    #     time.sleep(5)
    
        
    
