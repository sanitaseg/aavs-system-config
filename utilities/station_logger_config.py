_author__ = 'Gabriele Sorrenti'

configuration = {
    'tiles': None,
    'time_delays' : None,
    'station': {
        'id': 0,
        'name': 'EMI_test',
        'number_of_antennas': 16,
        'program': False,
        'initialise': False,
        'program_cpld': False,
        'enable_test': False,
        'qsfp_detection': 'none',
        'start_beamformer': True,
        'bitfile': '/home/user/itpm-test-layer/bitstream/itpm_v1_5_tpm_test_wrap_sbf520.bit',
        'single_tile_mode': True,
        'channel_truncation': 0, 
        'channel_integration_time': 1.5, 
        'beam_integration_time': -1, 
        'equalize_preadu': False, 
        'default_preadu_attenuation': 0, 
        'beamformer_scaling': 4, 
        'pps_delays': [0, 0], 
        'use_internal_pps': False,
        }, 
    'observation': {
        'bandwidth': 300000000.0, 
        'start_frequency_channel': 50000000.0}, 
        'network': {
            'tile_40g_subnet': None, 
            'active_40g_qsfp': 'port1-only', 
            'lmc': {
                'tpm_cpld_port': 10000, 'lmc_ip': '10.0.10.1', 
                'use_teng': False, 'lmc_port': 4660, 
                'integrated_data_ip': '10.0.10.1', 
                'integrated_data_port': 5000, 
                'use_teng_integrated': False
                },
            'csp_ingest': {
                'src_ip': None,
                'dst_mac': 40175250939192, 
                'src_port': None, 
                'dst_port': 4660, 
                'dst_ip': '10.0.0.98', 
                'src_mac': None
                },
        },
    }

subrack_values = {
    "temperatures" : {
        "SMM1":None,
        "SMM2":None,
        "BKPLN1":None,
        "BKPLN2":None,
    },
    "plls": {
        "BoardPllLock":None,
    },
    "fans" : {
        "speed" : {
            "FAN1":None,
            "FAN2":None,
            "FAN3":None,
            "FAN4":None,
        }
    },
    "psus" :{
        "temp_inlet": {
            "PSU1" : None,
            "PSU2" : None,
        },
        # "power_in": {
        #     "PSU1" : None,
        #     "PSU2" : None,
        # },
    },
    "slots" :{
        "powers" : {
            "SLOT1" : None,
            "SLOT2" : None,
            "SLOT3" : None,
            "SLOT4" : None,
            "SLOT5" : None,
            "SLOT6" : None,
            "SLOT7" : None,
            "SLOT8" : None,
        }
    }
}

station_values = {
    'temperatures': [
            'board',
            'FPGA0',
            'FPGA1',
    ],
}

def station_method(obj):
    return {
        'get_pps_delay' : obj.get_pps_delay,
        'get_adc_rms' : obj.get_adc_rms,
    }

station_register = [
    'board.regfile.pll_lol',
    'fpga1.pps_manager.pps_count',
    'fpga2.pps_manager.pps_count',
]