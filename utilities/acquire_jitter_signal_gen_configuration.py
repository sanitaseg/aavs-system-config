import time
import numpy as np
import logging
import sys

te_tpm_mapping=[{}]*32
te_tpm_mapping[0]  = { 'rfj_ch' :  8, 'te_tpm' : 5, 'tpm_fibre' :  9, 'tpm_pol' : 1 }
te_tpm_mapping[1]  = { 'rfj_ch' :  9, 'te_tpm' : 5, 'tpm_fibre' :  9, 'tpm_pol' : 2 }
te_tpm_mapping[2]  = { 'rfj_ch' : 10, 'te_tpm' : 6, 'tpm_fibre' : 10, 'tpm_pol' : 1 }
te_tpm_mapping[3]  = { 'rfj_ch' : 11, 'te_tpm' : 6, 'tpm_fibre' : 10, 'tpm_pol' : 2 }
te_tpm_mapping[4]  = { 'rfj_ch' : 12, 'te_tpm' : 7, 'tpm_fibre' : 11, 'tpm_pol' : 1 }
te_tpm_mapping[5]  = { 'rfj_ch' : 13, 'te_tpm' : 7, 'tpm_fibre' : 11, 'tpm_pol' : 2 }
te_tpm_mapping[6]  = { 'rfj_ch' : 14, 'te_tpm' : 8, 'tpm_fibre' : 12, 'tpm_pol' : 1 }
te_tpm_mapping[7]  = { 'rfj_ch' : 15, 'te_tpm' : 8, 'tpm_fibre' : 12, 'tpm_pol' : 2 }
te_tpm_mapping[8]  = { 'rfj_ch' :  9, 'te_tpm' : 5, 'tpm_fibre' : 13, 'tpm_pol' : 2 }
te_tpm_mapping[9]  = { 'rfj_ch' :  8, 'te_tpm' : 5, 'tpm_fibre' : 13, 'tpm_pol' : 1 }
te_tpm_mapping[10] = { 'rfj_ch' : 11, 'te_tpm' : 6, 'tpm_fibre' : 14, 'tpm_pol' : 2 }
te_tpm_mapping[11] = { 'rfj_ch' : 10, 'te_tpm' : 6, 'tpm_fibre' : 14, 'tpm_pol' : 1 }
te_tpm_mapping[12] = { 'rfj_ch' : 13, 'te_tpm' : 7, 'tpm_fibre' : 15, 'tpm_pol' : 2 }
te_tpm_mapping[13] = { 'rfj_ch' : 12, 'te_tpm' : 7, 'tpm_fibre' : 15, 'tpm_pol' : 1 }
te_tpm_mapping[14] = { 'rfj_ch' : 15, 'te_tpm' : 8, 'tpm_fibre' : 16, 'tpm_pol' : 2 }
te_tpm_mapping[15] = { 'rfj_ch' : 14, 'te_tpm' : 8, 'tpm_fibre' : 16, 'tpm_pol' : 1 }
te_tpm_mapping[16] = { 'rfj_ch' :  0, 'te_tpm' : 1, 'tpm_fibre' :  1, 'tpm_pol' : 1 }
te_tpm_mapping[17] = { 'rfj_ch' :  1, 'te_tpm' : 1, 'tpm_fibre' :  1, 'tpm_pol' : 2 }
te_tpm_mapping[18] = { 'rfj_ch' :  2, 'te_tpm' : 2, 'tpm_fibre' :  2, 'tpm_pol' : 1 }
te_tpm_mapping[19] = { 'rfj_ch' :  3, 'te_tpm' : 2, 'tpm_fibre' :  2, 'tpm_pol' : 2 }
te_tpm_mapping[20] = { 'rfj_ch' :  4, 'te_tpm' : 3, 'tpm_fibre' :  3, 'tpm_pol' : 1 }
te_tpm_mapping[21] = { 'rfj_ch' :  5, 'te_tpm' : 3, 'tpm_fibre' :  3, 'tpm_pol' : 2 }
te_tpm_mapping[22] = { 'rfj_ch' :  6, 'te_tpm' : 4, 'tpm_fibre' :  4, 'tpm_pol' : 1 }
te_tpm_mapping[23] = { 'rfj_ch' :  7, 'te_tpm' : 4, 'tpm_fibre' :  4, 'tpm_pol' : 2 }
te_tpm_mapping[24] = { 'rfj_ch' :  1, 'te_tpm' : 1, 'tpm_fibre' :  5, 'tpm_pol' : 2 }
te_tpm_mapping[25] = { 'rfj_ch' :  0, 'te_tpm' : 1, 'tpm_fibre' :  5, 'tpm_pol' : 1 }
te_tpm_mapping[26] = { 'rfj_ch' :  3, 'te_tpm' : 2, 'tpm_fibre' :  6, 'tpm_pol' : 2 }
te_tpm_mapping[27] = { 'rfj_ch' :  2, 'te_tpm' : 2, 'tpm_fibre' :  6, 'tpm_pol' : 1 }
te_tpm_mapping[28] = { 'rfj_ch' :  5, 'te_tpm' : 3, 'tpm_fibre' :  7, 'tpm_pol' : 2 }
te_tpm_mapping[29] = { 'rfj_ch' :  4, 'te_tpm' : 3, 'tpm_fibre' :  7, 'tpm_pol' : 1 }
te_tpm_mapping[30] = { 'rfj_ch' :  7, 'te_tpm' : 4, 'tpm_fibre' :  8, 'tpm_pol' : 2 }
te_tpm_mapping[31] = { 'rfj_ch' :  6, 'te_tpm' : 4, 'tpm_fibre' :  8, 'tpm_pol' : 1 }

if __name__ == "__main__":
    """
    Standalone program. 
    Connect and optionally initialises the station
    Executes the scan
    Saves results in a data file. 
    """
    from optparse import OptionParser
    from sys import argv, stdout
    #
    # Parser accepts all parameters for station
    #
    parser = OptionParser(usage="usage: %station [options]")
    
    # Specific options
    parser.add_option("--signal-gen-ip", default=None, 
        help="ip/hostname of signal generator")
    parser.add_option("--te-tpm-ip", default=None, 
        help="ip/hostname of TE_TPM")
    parser.add_option("--tone_frequency", action="store", 
        dest="start_frequency_channel",
        type="float", default=None, help="Tone frequency")
    
    (conf, args) = parser.parse_args(argv[1:])


    ampl_dBm = "18.0"

    if conf.te_tpm_ip is not None:
        sys.path.insert(0,'/home/user/itpm-test-layer_tpm_refactoring/')
        from itpm_rf_jig_sw import *
        filter = 0
        att = 10
        adu_ch = 14
        ampl_dBm = "-20"
        rfjig = RF_Jig(ip=conf.te_tpm_ip)
        rfjig.set_input_mode(Inputmode.ext_gen)
        rfjig.set_filter(filter)
        rfjig.set_attenuation(att)
        #rfjig.select_out_line(te_tpm_mapping[adu_ch]['rfj_ch'])
        for i in range(16):
            rfjig.select_out_line(i,disable_others=False)

    if conf.signal_gen_ip is not None:
        sys.path.insert(0,'/home/user/itpm-test-layer_signal_geneator_dev/')
        from function_generator import *
        signal_gen = rschwartz_sig_gen.RhodeSW_Sig_Gen()
        signal_gen.connect(conf.signal_gen_ip)
        print("Signal generator ID: " + str((signal_gen.get_id()[0]).decode('utf-8')), end="")
        signal_gen.en_output("0")
        time.sleep(0.5)
        f_Hz = str(conf.start_frequency_channel)
        
        signal_gen.set_frequency(f_Hz)
        print(f"Set output to sin: {float(f_Hz)/1e6} MHz at {ampl_dBm} dBm")
        signal_gen.set_power(ampl_dBm)
        time.sleep(0.5)
        signal_gen.en_output("1")
    
    