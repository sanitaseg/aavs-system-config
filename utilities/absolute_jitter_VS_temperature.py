import os
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import tabulate
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import r2_score
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--temp-key",  default="temperatures.BKPLN1")
    parser.add_argument("--start-time", default="2025-02-13T16:40")
    parser.add_argument("--end-time", default="2025-02-13T17:50")
    parser.add_argument("--subrack", default="subrack-1-1")
    parser.add_argument("--slot", default="slot-2")
    parser.add_argument("--ch", default="8")
    
    opt = parser.parse_args()

    token = os.environ.get("INFLUXDB_TOKEN")
    org = "sanitaseg"
    url = "http://localhost:8086"
    bucket="server_room"

    client = influxdb_client.InfluxDBClient(
        url=url,
        token=token,
        org=org
    )

    # Query script
    query_api = client.query_api()
    # query = 'from(bucket: "server_room")\
    #   |> range(start: 2025-02-10T15:07:00Z, stop: 2025-02-10T15:12:00Z)\
    #   |> filter(fn: (r) => r["_measurement"] == "correlation")\
    #   |> filter(fn: (r) => r["SLOT"] == "subrack-1-1-slot-2")\
    #   |> filter(fn: (r) => r["CH"] == "9")\
    #   |> filter(fn: (r) => r["_field"] == "rel_jitters")'

    # opt.temp_key="temperatures.BKPLN1"
    # opt.start_time="2025-02-13T16:40"
    # opt.end_time="2025-02-13T17:50"
    temp_filter = f'r["_field"] == "{opt.temp_key}"'
    if opt.temp_key in ['temperatures.FPGA0', 'temperatures.FPGA1', 'temperatures.board']:
        temp_filter = f'r["_field"] == "{opt.temp_key}" and r["SLOT"] == "{opt.slot}"'
    query = f'from(bucket: "server_room")\
        |> range(start: {opt.start_time}:00Z, stop: {opt.end_time}:00Z)\
        |> filter(fn: (r) => r["_measurement"] == "correlation" or r["_measurement"] == "{opt.subrack}")\
        |> filter(fn: (r) => (r["_field"] == "abs_jitters" and r["SLOT"] == "{opt.subrack}-{opt.slot}" and r["CH"] == "{opt.ch}") or ({temp_filter}))\
        |> aggregateWindow(every: 120s, fn: mean, createEmpty: false)\
        |> yield(name: "mean")'
    print(query)
    result = query_api.query(org=org, query=query)
    # print(result)
    results = {}
    for table in result:
        for record in table.records:
            _time=record.get_time().strftime("%Y%m%d,%H:%M")
            if _time not in results:
                results[_time]={}
            results[_time][record.get_field()]=record.get_value()
    # print(results)
    headers = list(results[next(iter(results))].keys())
    # print(headers)
    headers.insert(0,'time')
    results = [[name, *inner.values()] for name, inner in results.items()]
    for result in results:
      print(result)
    print(tabulate.tabulate(results, headers=headers, tablefmt="pipe"))
    df = pd.DataFrame(results, columns=headers)
    plt.figure(figsize=(12, 6))
    plt.plot(df[opt.temp_key],df["abs_jitters"],'.',label="measure")
    coef  = np.polyfit(df[opt.temp_key],df["abs_jitters"], 1)
    poly1d_fn = np.poly1d(coef)
    r2=r2_score(df["abs_jitters"],poly1d_fn(df[opt.temp_key]))
    plt.plot(df[opt.temp_key], poly1d_fn(df[opt.temp_key]), '-', label=f"m = {round(coef[0],1)}, q = {round(coef[1],1)}, R2 = {round(r2,4)}")
    plt.xlabel(f"temperature [°C]")
    plt.ylabel("absolute jitter [ps]")
    plt.title(f"Absolute Jitter VS {opt.temp_key}\n{opt.subrack}-{opt.slot}-ch{opt.ch}")
    plt.grid(True)
    plt.legend()
    print(coef)

    png_file=f"{opt.subrack}-{opt.slot}-ch{opt.ch}_{opt.start_time}_{opt.end_time}_absolute_jitter_VS_{opt.temp_key}.png"
    png_file=png_file.replace(":",".")
    print(f"Plot saved to {png_file}")
    plt.savefig(png_file, dpi=600)